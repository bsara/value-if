/**
 * @license
 *
 * ISC License (ISC)
 *
 * Copyright (c) 2017, Brandon D. Sara (https://bsara.pro/)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], factory);
    return;
  }
  if (typeof exports === 'object') {
    module.exports = factory();
    return;
  }
  var exported = factory();

  root.valueIf    = exported.valueIf
  root.valueIfAny = exported.valueIfAny;
})(this, function() {
  /**
   * @param  {*}            value      - Value to be returned if all of the items in
   *                                     `conditions` evalute to `true`.
   * @param  {(...*|Array)} conditions - Conditions used to determine whether to return
   *                                     `value` or `undefined`.
   *
   * @returns {?*} `value` if ALL of the given `conditions` evaluate to `true`; otherwise,
   *               returns `undefined`.
   */
  function valueIf(value) {
    return (arguments.length > 1 && _areAllTrue(Array.prototype.slice.call(arguments, 1)))
             ? value
             : undefined;
  }



  /**
   * @param  {*}            value      - Value to be returned if any of the items in
   *                                     `conditions` evalute to `true`.
   * @param  {(...*|Array)} conditions - Conditions used to determine whether to return
   *                                     `value` or `undefined`.
   *
   * @returns {?*} `value` if ANY of the given `conditions` evaluate to `true`; otherwise,
   *               returns `undefined`.
   */
  function valueIfAny(value) {
    return (arguments.length > 1 && _areAnyTrue(Array.prototype.slice.call(arguments, 1)))
             ? value
             : undefined;
  }




  // region Private Helpers

  /** @private */
  function _areAllTrue(conditions) {
    if (!Array.isArray(conditions)) {
      return Boolean(conditions);
    }

    for (var i = 0; i < conditions.length; i++) {
      if (!_areAllTrue(conditions[i])) {
        return false;
      }
    }

    return true;
  }


  /** @private */
  function _areAnyTrue(conditions) {
    if (!Array.isArray(conditions)) {
      return Boolean(conditions);
    }

    for (var i = 0; i < conditions.length; i++) {
      if (_areAnyTrue(conditions[i])) {
        return true;
      }
    }

    return false;
  }

  // endregion



  return {
    valueIf:    valueIf,
    valueIfAny: valueIfAny
  };
});
